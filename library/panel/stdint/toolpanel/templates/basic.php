<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
 ?>
<div id="tpanel<?php echo $vars->_pnid ?>">
	<div class="xsmallfont" id="tpanel<?php echo $vars->_pnid ?>-title" style="text-weight: bold">
	</div>
	<div id="tpanel<?php echo $vars->_pnid ?>-body">
	Nothing Selected
	</div>
</div>
