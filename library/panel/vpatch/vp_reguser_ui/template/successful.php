<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
?>
<div class="register">
<div class="rform vfloat-left">
	<center style="color: #7D9E05; font-weight: bold;">
	<img src="<?php echo $vars->_fallback->mediag; ?>/ngtick-128.png" /><br /><br />
	Successfully registered!
	</center>
	<div style="font-size: small; text-align: center; margin-top: 50px;">
	A message should have been sent to your university email inbox with an activation key.
	</div>
</div>

<div class="rdata vfloat-left">
	<h3>Welcome to Kura registration!</h3>
	<p>
	So, a quick reminder about what all this stuff is:
	</p>

	<p>
	Kura is the platform being used in conjunction with SS436.The platform itself is going to evolve
	with student (and staff) use. This means that your feedback is not only deeply appreciated, it is 
	an integeral part of the process. It is certainly not meant to be painful to use so if you find it
	functionally confusing, irritating or even traumatic then please tell us! Like-wise if you would like
	to see suggest features to be added then pop us a message!
	</p>

	<p>
	At the moment it is a closed system, meaning only yourself and the rest of the module have access. It is also
	separate to the normal student website which are the reason's why we need you to enter your university email address.
	</p>
	
	<p>
	To be clear- we are <b>not</b> going to send you nonsense, we are <b>not</b> going to pass on details; it is only used 
	for handling registration and administration.
	</p>
</div>
</div>
