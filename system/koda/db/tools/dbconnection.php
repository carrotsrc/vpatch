<?php
/* Copyright 2014, Charlie Fyvie-Gauld (Carrotsrg.org)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

	class DBConnection
	{
		protected $connection;
		
		public function __construct()
		{
		
		}
		
		public function connect($username, $password)
		{
		
		}
		
		public function connectAtServer($server, $username, $password)
		{
		
		}
		
		public function selectDatabase($database)
		{
		
		}
		
		public function sendQuery($query, $raw = true, $assoc = true)
		{
		
		}
		
		public function getLastId()
		{
		
		}		
	}
?>
